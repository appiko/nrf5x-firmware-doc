var searchData=
[
  ['nrf_20specific_20utilities',['nRF specific utilities',['../group__group__nrf__util.html',1,'']]],
  ['narrow_5foffset_5fpin',['NARROW_OFFSET_PIN',['../group__board__detect__rev1.html#gaa428405dcf2f2e8fda8a65e56cd12d9c',1,'detect_rev1.h']]],
  ['narrow_5fsignal_5fpin',['NARROW_SIGNAL_PIN',['../group__board__detect__rev1.html#ga01209ffc8da85fc22874436a0316002c',1,'detect_rev1.h']]],
  ['narrow_5fvdd_5fpin',['NARROW_VDD_PIN',['../group__board__detect__rev1.html#ga0135c514bbc353ac4b32d204129cb3a4',1,'detect_rev1.h']]],
  ['nfc_5fcircuitry',['NFC_CIRCUITRY',['../group__board__bluey__1v1.html#ga5d712aa5759e862cef9313767e60f5fd',1,'NFC_CIRCUITRY():&#160;bluey_1v1.h'],['../group__board__detect__rev1.html#ga5d712aa5759e862cef9313767e60f5fd',1,'NFC_CIRCUITRY():&#160;detect_rev1.h'],['../group__board__pca10040.html#ga5d712aa5759e862cef9313767e60f5fd',1,'NFC_CIRCUITRY():&#160;pca10040.h']]]
];
