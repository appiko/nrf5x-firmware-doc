var searchData=
[
  ['pca10028_20nrf51_2ddk',['PCA10028 nRF51-DK',['../group__board__pca10028.html',1,'']]],
  ['pca10040_20nrf52_2ddk',['PCA10040 nRF52-DK',['../group__board__pca10040.html',1,'']]],
  ['peripheral_20modules',['Peripheral Modules',['../group__group__peripheral__modules.html',1,'']]],
  ['platforms',['Platforms',['../group__group__platform.html',1,'']]],
  ['profiler_20timer',['Profiler timer',['../group__group__profiler__timer.html',1,'']]],
  ['print_5ftime',['PRINT_TIME',['../group__group__profiler__timer.html#gac3f5ae786ea023082d01a5d0d9503908',1,'profiler_timer.h']]],
  ['printfcomma',['printfcomma',['../group__group__profiler__timer.html#ga54a814f2e5df087fa43aee4dcfeaa8bf',1,'printfcomma(uint32_t num):&#160;profiler_timer.c'],['../group__group__profiler__timer.html#ga54a814f2e5df087fa43aee4dcfeaa8bf',1,'printfcomma(uint32_t num):&#160;profiler_timer.c']]],
  ['profile_5fstart',['PROFILE_START',['../group__group__profiler__timer.html#gaa402baa5f9ea438ec090db1e77961ecc',1,'profiler_timer.h']]],
  ['profile_5fstop',['PROFILE_STOP',['../group__group__profiler__timer.html#gab35dbcc10bc85057cfc3cd925cb9fd96',1,'profiler_timer.h']]],
  ['profiler_5ftimer',['PROFILER_TIMER',['../group__group__profiler__timer.html#ga250c953f362aa38b79a678ac14278326',1,'profiler_timer.h']]],
  ['profiler_5ftimer_5fdeinit',['profiler_timer_deinit',['../group__group__profiler__timer.html#ga939a8aec9bac8bce541d60f29b6ae148',1,'profiler_timer_deinit():&#160;profiler_timer.c'],['../group__group__profiler__timer.html#ga939a8aec9bac8bce541d60f29b6ae148',1,'profiler_timer_deinit():&#160;profiler_timer.c']]],
  ['profiler_5ftimer_5finit',['profiler_timer_init',['../group__group__profiler__timer.html#ga285808b4b2b54106addceeda537a73c1',1,'profiler_timer_init(void):&#160;profiler_timer.c'],['../group__group__profiler__timer.html#ga285808b4b2b54106addceeda537a73c1',1,'profiler_timer_init(void):&#160;profiler_timer.c']]],
  ['profiler_5ftimer_5fis_5fon',['profiler_timer_is_on',['../group__group__profiler__timer.html#ga69cb2d6f5be0bc7d7c196243cf7d7665',1,'profiler_timer_is_on(void):&#160;profiler_timer.c'],['../group__group__profiler__timer.html#ga69cb2d6f5be0bc7d7c196243cf7d7665',1,'profiler_timer_is_on(void):&#160;profiler_timer.c']]]
];
