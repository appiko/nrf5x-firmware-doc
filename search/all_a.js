var searchData=
[
  ['millisecond_20timer',['Millisecond timer',['../group__group__ms__timer.html',1,'']]],
  ['main',['main',['../group__bluey__demo.html#ga840291bc02cba5474a4cb46a9b9566fe',1,'main(void):&#160;main.c'],['../group__hello__world__blinky.html#ga840291bc02cba5474a4cb46a9b9566fe',1,'main(void):&#160;main.c'],['../group__hello__world__rtt.html#ga840291bc02cba5474a4cb46a9b9566fe',1,'main(void):&#160;main.c'],['../group__saadc__logger.html#ga840291bc02cba5474a4cb46a9b9566fe',1,'main(void):&#160;main.c']]],
  ['max',['MAX',['../group__group__common__util.html#gafa99ec4acc4ecb2dc3c2d05da15d0e3f',1,'common_util.h']]],
  ['min',['MIN',['../group__group__common__util.html#ga3acffbd305ee72dcd4593c0d8af64a4f',1,'common_util.h']]],
  ['ms_5frepeated_5fcall',['MS_REPEATED_CALL',['../group__group__ms__timer.html#gga162a9b8dbe3b840ba33f56fdf6df05d6a7e9fc36bd2918ee0bc516874b72af7ac',1,'ms_timer.h']]],
  ['ms_5fsingle_5fcall',['MS_SINGLE_CALL',['../group__group__ms__timer.html#gga162a9b8dbe3b840ba33f56fdf6df05d6a89efc7900dcac27eef2f1e7b48f75c65',1,'ms_timer.h']]],
  ['ms_5ftimer0',['MS_TIMER0',['../group__group__ms__timer.html#gga5dd28145e36ca5223dbf142fdb2a8f80a4e981321c0c331827ae00479467a6147',1,'ms_timer.h']]],
  ['ms_5ftimer1',['MS_TIMER1',['../group__group__ms__timer.html#gga5dd28145e36ca5223dbf142fdb2a8f80a8acfb5a79ceb7151df2fdae66436f3cd',1,'ms_timer.h']]],
  ['ms_5ftimer2',['MS_TIMER2',['../group__group__ms__timer.html#gga5dd28145e36ca5223dbf142fdb2a8f80a82da1cd23591a96386427076137e3c50',1,'ms_timer.h']]],
  ['ms_5ftimer_5fcc_5fcount',['MS_TIMER_CC_COUNT',['../group__group__ms__timer.html#ga3c782c4b9984318c9a5e580f66658273',1,'ms_timer.h']]],
  ['ms_5ftimer_5ffreq',['MS_TIMER_FREQ',['../group__group__ms__timer.html#ga31fa93ab780bce80726fb31315fc5988',1,'ms_timer.h']]],
  ['ms_5ftimer_5fget_5fon_5fstatus',['ms_timer_get_on_status',['../group__group__ms__timer.html#ga1549bb6e43fc8302ccb52c346c712d43',1,'ms_timer_get_on_status(ms_timer_num id):&#160;ms_timer.c'],['../group__group__ms__timer.html#ga1549bb6e43fc8302ccb52c346c712d43',1,'ms_timer_get_on_status(ms_timer_num id):&#160;ms_timer.c']]],
  ['ms_5ftimer_5finit',['ms_timer_init',['../group__group__ms__timer.html#ga49148281c1dad97bca001a2d50fcf467',1,'ms_timer_init(uint32_t irq_priority):&#160;ms_timer.c'],['../group__group__ms__timer.html#ga49148281c1dad97bca001a2d50fcf467',1,'ms_timer_init(uint32_t irq_priority):&#160;ms_timer.c']]],
  ['ms_5ftimer_5fmax',['MS_TIMER_MAX',['../group__group__ms__timer.html#gga5dd28145e36ca5223dbf142fdb2a8f80ada8df48b91dbbd473914dfc26e376cad',1,'ms_timer.h']]],
  ['ms_5ftimer_5fmode',['ms_timer_mode',['../group__group__ms__timer.html#ga162a9b8dbe3b840ba33f56fdf6df05d6',1,'ms_timer.h']]],
  ['ms_5ftimer_5fnum',['ms_timer_num',['../group__group__ms__timer.html#ga5dd28145e36ca5223dbf142fdb2a8f80',1,'ms_timer.h']]],
  ['ms_5ftimer_5frtc_5fused',['MS_TIMER_RTC_USED',['../group__group__ms__timer.html#ga5987d96e39bce107d62814e5176e7e91',1,'ms_timer.h']]],
  ['ms_5ftimer_5fstart',['ms_timer_start',['../group__group__ms__timer.html#ga91f80954af6e32f13b07c40b0245db53',1,'ms_timer_start(ms_timer_num id, ms_timer_mode mode, uint32_t ticks, void(*handler)(void)):&#160;ms_timer.c'],['../group__group__ms__timer.html#ga91f80954af6e32f13b07c40b0245db53',1,'ms_timer_start(ms_timer_num id, ms_timer_mode mode, uint32_t ticks, void(*handler)(void)):&#160;ms_timer.c']]],
  ['ms_5ftimer_5fstop',['ms_timer_stop',['../group__group__ms__timer.html#ga1e5c0b71b07c93faa521f4c0d927f281',1,'ms_timer_stop(ms_timer_num id):&#160;ms_timer.c'],['../group__group__ms__timer.html#ga1e5c0b71b07c93faa521f4c0d927f281',1,'ms_timer_stop(ms_timer_num id):&#160;ms_timer.c']]]
];
