var searchData=
[
  ['saadc_5fchannel_5funinit',['saadc_channel_uninit',['../group__group__saadc.html#ga61d04833d4cc901bf85ede2f14344647',1,'hal_saadc.h']]],
  ['saadc_5fcontinuous_5fsampling_5frate_5fset',['saadc_continuous_sampling_rate_set',['../group__group__saadc.html#ga9459a3299ad0a4bb491adc193bd46932',1,'hal_saadc.h']]],
  ['saadc_5firqhandler',['SAADC_IRQHandler',['../group__saadc__logger.html#ga103a75ff5551ffbdb68a3ce4a90da201',1,'main.c']]],
  ['saadc_5fsampling_5ftask_5fmode_5fset',['saadc_sampling_task_mode_set',['../group__group__saadc.html#ga299b0f7a892ed3d951ace2be00b62113',1,'hal_saadc.h']]],
  ['simple_5fadc_5fget_5fvalue',['simple_adc_get_value',['../group__group__simple__adc.html#ga845022b96aea4b5194bb2a3fd109af1d',1,'simple_adc_get_value(simple_adc_gain_t gain, simple_adc_input_t pin):&#160;simple_adc.c'],['../group__group__simple__adc.html#ga845022b96aea4b5194bb2a3fd109af1d',1,'simple_adc_get_value(simple_adc_gain_t gain, simple_adc_input_t pin):&#160;simple_adc.c']]]
];
