var indexSectionsWithContent =
{
  0: "abcdefghilmnprstuw",
  1: "hs",
  2: "ahlmprsu",
  3: "aefis",
  4: "ahlmst",
  5: "hlmst",
  6: "abcdghmnpstu",
  7: "acst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "functions",
  3: "variables",
  4: "enums",
  5: "enumvalues",
  6: "groups",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Functions",
  3: "Variables",
  4: "Enumerations",
  5: "Enumerator",
  6: "Modules",
  7: "Pages"
};

