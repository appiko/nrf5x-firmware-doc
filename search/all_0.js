var searchData=
[
  ['address',['address',['../structhal__twim__init__config__t.html#ab97332832052ba9ef94e47d92a40205a',1,'hal_twim_init_config_t']]],
  ['app_5ferror_5fcheck',['APP_ERROR_CHECK',['../group__group__app__error.html#ga82d00a810dcea7dcc6af469461fb254c',1,'app_error.h']]],
  ['app_5ferror_5fhandler',['app_error_handler',['../group__group__app__error.html#gad8b5b293dfa06cbbfeb03aaaaa2772cf',1,'app_error_handler(uint32_t error_code, uint32_t line_num, const uint8_t *p_file_name):&#160;app_error.c'],['../group__group__app__error.html#gad8b5b293dfa06cbbfeb03aaaaa2772cf',1,'app_error_handler(uint32_t error_code, uint32_t line_num, const uint8_t *p_file_name):&#160;app_error.c']]],
  ['app_5firq_5fpriority_5ft',['app_irq_priority_t',['../group__group__nrf__util.html#ga1a193131d1d5911b1ec53f9541841b1b',1,'nrf_util.h']]],
  ['appln_5fsaadc_5firq_5fpriority',['APPLN_SAADC_IRQ_PRIORITY',['../group__saadc__logger.html#ga218767d485dcb049a3d93e742ca497d3',1,'main.c']]],
  ['appln_5fsaadc_5foversampling',['APPLN_SAADC_OVERSAMPLING',['../group__saadc__logger.html#gadecddc10792126536e13e3ff209c0503',1,'main.c']]],
  ['appln_5fsaadc_5fresolution',['APPLN_SAADC_RESOLUTION',['../group__saadc__logger.html#ga27d73857e308a5260a964ba047c4a6fc',1,'main.c']]],
  ['array_5fsize',['ARRAY_SIZE',['../group__group__common__util.html#ga3c7c6a69f690fc8d2abf0e385280a532',1,'common_util.h']]],
  ['assert',['ASSERT',['../group__group__assertion.html#ga87e006a00875d2e518652108f6cb5790',1,'nrf_assert.h']]],
  ['assert_5fnrf_5fcallback',['assert_nrf_callback',['../group__group__assertion.html#gadec561788a76b8a63ad9663625a25d78',1,'assert_nrf_callback(uint16_t line_num, const uint8_t *file_name):&#160;nrf_assert.c'],['../group__group__assertion.html#gadec561788a76b8a63ad9663625a25d78',1,'assert_nrf_callback(uint16_t line_num, const uint8_t *file_name):&#160;nrf_assert.c']]],
  ['a_20bluey_20demo',['A Bluey demo',['../group__bluey__demo.html',1,'']]],
  ['appiko_20detect_20revision_201',['Appiko Detect Revision 1',['../group__board__detect__rev1.html',1,'']]],
  ['application_20error_20handler',['Application error handler',['../group__group__app__error.html',1,'']]],
  ['applications',['Applications',['../group__group__appln.html',1,'']]],
  ['assertions_20and_20error_20handling',['Assertions and Error Handling',['../group__group__assert__error.html',1,'']]],
  ['appiko_20nrf52_20firmware',['Appiko nRF52 firmware',['../index.html',1,'']]]
];
