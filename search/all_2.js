var searchData=
[
  ['ceil_5fdiv',['CEIL_DIV',['../group__group__common__util.html#ga7aaa817cfd7ed354f09db5675ffe1427',1,'common_util.h']]],
  ['concat_5f2',['CONCAT_2',['../group__group__common__util.html#ga6905a64dacf8e16d094d1ae878db4700',1,'common_util.h']]],
  ['concat_5f2_5f',['CONCAT_2_',['../group__group__common__util.html#gac0b63152aca954175cdcd67ee5f30349',1,'common_util.h']]],
  ['concat_5f3',['CONCAT_3',['../group__group__common__util.html#ga8f2d8588e43cf08fbc3777dd842b6b80',1,'common_util.h']]],
  ['concat_5f3_5f',['CONCAT_3_',['../group__group__common__util.html#ga0aa50c142a7d3bca8c1ea8a094d2254d',1,'common_util.h']]],
  ['clocks_20hal',['Clocks HAL',['../group__group__clocks.html',1,'']]],
  ['codebase',['Codebase',['../group__group__codebase.html',1,'']]],
  ['common_20utilities',['Common utilities',['../group__group__common__util.html',1,'']]],
  ['changes_20made_20to_20the_20nordicsemi_27s_20nrf5_20sdk',['Changes made to the NordicSemi&apos;s nRF5 SDK',['../sdk_changelog.html',1,'']]]
];
