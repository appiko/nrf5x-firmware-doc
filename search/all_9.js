var searchData=
[
  ['leds_5factive_5fstate',['LEDS_ACTIVE_STATE',['../group__board__bluey__1v1.html#ga8315d68f769a028273fb5656d720196b',1,'LEDS_ACTIVE_STATE():&#160;bluey_1v1.h'],['../group__board__pca10028.html#ga8315d68f769a028273fb5656d720196b',1,'LEDS_ACTIVE_STATE():&#160;pca10028.h'],['../group__board__pca10040.html#ga8315d68f769a028273fb5656d720196b',1,'LEDS_ACTIVE_STATE():&#160;pca10040.h']]],
  ['lfclk_5fdeinit',['lfclk_deinit',['../group__group__clocks.html#gaacf6895e77d4c3479e204b9dc0938ef2',1,'lfclk_deinit(void):&#160;hal_clocks.c'],['../group__group__clocks.html#gaacf6895e77d4c3479e204b9dc0938ef2',1,'lfclk_deinit(void):&#160;hal_clocks.c']]],
  ['lfclk_5ffreq',['LFCLK_FREQ',['../group__group__clocks.html#gaf1aa923a7fd7949c9e88d3ed33b8d46e',1,'hal_clocks.h']]],
  ['lfclk_5finit',['lfclk_init',['../group__group__clocks.html#ga3ef52b74a80bf5640f74a89275baabf1',1,'lfclk_init(lfclk_src_t lfclk_src):&#160;hal_clocks.c'],['../group__group__clocks.html#ga3ef52b74a80bf5640f74a89275baabf1',1,'lfclk_init(lfclk_src_t lfclk_src):&#160;hal_clocks.c']]],
  ['lfclk_5fsrc_5frc',['LFCLK_SRC_RC',['../group__group__clocks.html#ggaf7cd488f51efbd532e974a0ab36013d1a6091392e9b113f1fb61a30fefb3a7aec',1,'hal_clocks.h']]],
  ['lfclk_5fsrc_5fsynth',['LFCLK_SRC_Synth',['../group__group__clocks.html#ggaf7cd488f51efbd532e974a0ab36013d1a7e2befce393c94d3b288033615e6325e',1,'hal_clocks.h']]],
  ['lfclk_5fsrc_5ft',['lfclk_src_t',['../group__group__clocks.html#gaf7cd488f51efbd532e974a0ab36013d1',1,'hal_clocks.h']]],
  ['lfclk_5fsrc_5fxtal',['LFCLK_SRC_Xtal',['../group__group__clocks.html#ggaf7cd488f51efbd532e974a0ab36013d1a7566c04d20dd8373bf3f518899f18ac3',1,'hal_clocks.h']]],
  ['lfclk_5fxtal_5fpresent',['LFCLK_XTAL_PRESENT',['../group__board__bluey__1v1.html#gad7486b1e20bc18fcdfb889bd33a12353',1,'LFCLK_XTAL_PRESENT():&#160;bluey_1v1.h'],['../group__board__detect__rev1.html#gad7486b1e20bc18fcdfb889bd33a12353',1,'LFCLK_XTAL_PRESENT():&#160;detect_rev1.h'],['../group__board__pca10028.html#gad7486b1e20bc18fcdfb889bd33a12353',1,'LFCLK_XTAL_PRESENT():&#160;pca10028.h'],['../group__board__pca10040.html#gad7486b1e20bc18fcdfb889bd33a12353',1,'LFCLK_XTAL_PRESENT():&#160;pca10040.h']]]
];
